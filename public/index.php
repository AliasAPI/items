<?php

namespace AliasAPI\Items;

use AliasAPI\Items as Items;
use AliasAPI\Server as Server;

require_once('../vendor/aliasapi/frame/server/bootstrap.php');

$settings = Server\bootstrap();

$pod = Server\track($settings);
// 2DO+++ remove business logic from items?
Items\track($pod);
