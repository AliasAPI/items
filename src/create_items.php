<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function create_items(array $train): void
{
    $key_pairs = [];
    $items_created = 0;
    $response = [];

    if (! isset($train['action'])
        || $train['action'] !== 'create items') {
        return;
    }

    $train['table'] = ($train['table']) ?? 'items';

    if (! isset($train['items'])
        || empty($train['items'])) {
        Messages\respond(400, ["The items is not set."]);
    }

    foreach ($train['items'] as $index => $pairs) {
        $item_keys = \array_keys($pairs);

        foreach ($item_keys as $index => $column) {
            $key_pairs[$column] = $pairs[$column];
        }

        $rows = CrudTable\read_rows($train['table'], $key_pairs);

        if (empty($rows)) {
            CrudTable\create_row($train['table'], $key_pairs);
            $items_created++;
        }

        // Prevent row settings from applying to susequent row inserts
        unset($key_pairs);
    }

    $response['items'] = ["[$items_created] items created"];

    Messages\respond(201, $response);
}
