<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function delete_items(array $train): void
{
    $key_pairs = [];
    $row_count = 0;
    $response = [];

    if (! isset($train['action'])
        || $train['action'] !== 'delete items') {
        return;
    }

    $train['table'] = ($train['table']) ?? 'items';

    if (! isset($train['items'])
        || empty($train['items'])) {
        Messages\respond(400, ["The items to delete is not set."]);
    }

    foreach ($train['items'] as $index => $pairs) {
        $item_keys = \array_keys($pairs);

        foreach ($item_keys as $index => $column) {
            $key_pairs[$column] = $pairs[$column];
        }

        $count = CrudTable\delete_rows($train['table'], $key_pairs);

        $row_count += $count;

        // Prevent row settings from applying to susequent row deletions
        unset($key_pairs);
    }

    $response['items'] = ["[$row_count] items deleted"];

    Messages\respond(200, $response);
}
