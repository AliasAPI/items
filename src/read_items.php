<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function read_items(array $train): void
{
    $i = 0;
    $response = [];
    $key_pairs = [];

    if (! isset($train['action'])
        || $train['action'] !== 'read items') {
        return;
    }

    if (! isset($train['items'])
        || empty($train['items'])) {
        Messages\respond(400, ["The items is not set."]);
    }

    $train['table'] = ($train['table']) ?? 'items';

    $train['limit'] = ($train['limit']) ?? 1000;

    foreach ($train['items'] as $index => $pairs) {
        $item_keys = \array_keys($pairs);
        foreach ($item_keys as $index => $column) {
            $key_pairs[$column] = $pairs[$column];
        }

        $rows = CrudTable\read_rows($train['table'], $key_pairs, $train['limit']);

        if (! empty($rows)) {
            foreach ($rows as $index => $row) {
                // Use $i++ to append more rows
                $response['items'][$i++] = $row;
            }
        }

        // Prevent row settings from applying to susequent row reads
        unset($key_pairs);
    }

    Messages\respond(200, $response);
}
