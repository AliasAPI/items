<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\Messages as Messages;

function reduce_rows(array $train): void
{
    $response = [];
    $rows = [];

    if (! isset($train['unset'])
        || empty($train['unset'])) {
        return;
    }

    $body = Messages\get_body();

    $status_code = $body['status_code'] ?? '200';

    if (isset($body['message']['items'])
        && ! empty($body['message']['items'])) {
        foreach ($body['message']['items'] as $index => $row) {
            if (isset($row['id'])) {
                $id = $row['id'];
                foreach ($train['unset'] as $index => $unset) {
                    // Reduce the key pairs in each row result
                    unset($row[$unset]);
                }
                // Reduce row results by overwriting duplicate rows based on id
                $rows[$id] = $row;
            } else {
                foreach ($train['unset'] as $index => $unset) {
                    // Reduce the key pairs in each row result
                    unset($row[$unset]);
                }
                // The table does not have an id column
                $rows[] = $row;
            }
        }

        $response['items'] = $rows;

        Messages\respond(200, $response);
    }
}
