<?php

declare(strict_types=1);

namespace AliasAPI\Items;

function renew_request($train): array
{
    if (isset($train['renew_request'])) {
        return $train['renew_request'];
    } else {
        return $train;
    }
}
