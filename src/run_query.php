<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function run_query(array $train)
{
    $response = [];
    
    if (! isset($train['action'])
        || $train['action'] !== 'run query') {
        return;
    }

    if (! isset($train['items'])
        || empty($train['items'])) {
        Messages\respond(400, ["The items is not set."]);
    }

    foreach ($train['items'] as $key => $part) {
        if (! isset($part['bind_sql'])
            || empty($part['bind_sql'])) {
            Messages\respond(400, ["The bind_sql string is not set."]);
        }

        if (! isset($part['update'])) {
            Messages\respond(400, ["The update array is not set."]);
        }

        if (! isset($part['where'])) {
            Messages\respond(400, ["The where array is not set."]);
        }

        // Remove whitespace from the human-readable indentation by Atom
        $bind_sql = \preg_replace('/[ \t]+/', '<ws>', $part['bind_sql']);
        $bind_sql = \str_replace(array('n<ws>', '<ws>'), ' ', $bind_sql);

        CrudTable\query($bind_sql, $part['update'], $part['where']);
    }

    $response['items'] = ["query ran"];

    Messages\respond(200, $response);
}
