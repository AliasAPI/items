<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function setup_services($train)
{
    if (! isset($train['action'])
        || $train['action'] !== 'setup services') {
        return;
    }

    // tour: Default items table structure:
    $sql = "CREATE TABLE IF NOT EXISTS `items` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(250) NOT NULL,
            `item` varchar(250) NOT NULL,
            `note` varchar(250) NOT NULL,
            `time` datetime DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `uuid` (`uuid`(191),`item`(191),`note`(191),`time`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    CrudTable\query($sql);

    Messages\respond(201, ["ItemsService setup is complete."]);
}
