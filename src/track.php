<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\Items as Items;
use AliasAPI\Messages as Messages;

function track(array $train)
{
    Items\setup_services($train);

    Items\create_items($train);

    Items\update_items($train);

    Items\delete_items($train);

    Items\run_query($train);

    $train = Items\renew_request($train);

    Items\read_items($train);

    Items\reduce_rows($train);

    Messages\respond();
}
