<?php

declare(strict_types=1);

namespace AliasAPI\Items;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function update_items(array $train)
{
    $response = [];
    $row_count = 0;

    if (! isset($train['action'])
        || $train['action'] !== 'update items') {
        return;
    }

    $train['table'] = ($train['table']) ?? 'items';

    if (! isset($train['items'])
        || empty($train['items'])) {
        Messages\respond(400, ["The items to update is not set."]);
    }

    foreach ($train['items'] as $key => $array) {
        if (! isset($array['update'])
            || empty($array['update'])) {
            Messages\respond(400, ["The update array is not set."]);
        }

        if (! isset($array['where'])
            || empty($array['where'])) {
            Messages\respond(400, ["The where array is not set."]);
        }

        $count = CrudTable\update_rows($train['table'], $array['update'], $array['where']);

        $row_count += $count;
    }

    $response['items'] = ["[$row_count] items updated"];

    Messages\respond(200, $response);
}
