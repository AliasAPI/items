<?php

namespace AliasAPI\Tests;

use AliasAPI\Client as Client;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;
use \AllowDynamicProperties;

/**
 * Class MockClient
 * Autoloads files and configures a default request
 *
 * @package AliasAPI\Tests
 */
#[AllowDynamicProperties]
class CreateClient
{
    public $request = [];
    public $tag = '';

    /**
     * CreateClient autoloads files and sets additional default request parameters
     *
     * @param array $pod
     *
     * @return array
     */
    public function __construct($request)
    {
        require_once '/app/vendor/aliasapi/frame/client/track.php';

        $this->request = Client\track($request);

        $this->setTag();
    }

    public function sendRequest()
    {
        $response = Messages\request($this->request);

        return $response;
    }

    public function getPair()
    {
        $pair = CrudPair\get_pair_global();

        return $pair;
    }

    public function setTag()
    {
        $this->tag = Messages\get_request_tag();

        return $this->tag;
    }
}
