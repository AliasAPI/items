<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class CreatePairTests extends TestCase
{
    private $client;
    private $request;
    private $client_path;
    private $server_path;
    private $debug_path;
    private $new_request;
    private $say_path;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testShouldBeTrue()
    {
        $this->assertTrue(true);
    }

    public function testDeletePairFile()
    {
        // Delete the pair file because it is re-created in testCreatePairFile
        $request['action'] = 'delete pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        $this->assertEquals(200, $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('The pair file has been deleted.', $response['body'][0]);
    }

    public function testCreatePairFile()
    {
        $request['action'] = 'create pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals(201, $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('create pair file', $body['action']);
        $this->assertTrue(\array_key_exists('pair', $body));
        $this->assertEquals('TestClient', $body['pair']['client']);
        $this->assertEquals('ItemsService', $body['pair']['server']);
        $this->assertEquals(
            $this->client->request['pair']['client_url'],
            $body['pair']['client_url']
        );
        $this->assertEquals(
            $this->client->request['pair']['server_url'],
            $body['pair']['server_url']
        );
       // $this->assertStringContainsString('items', $body['pair']['server_url']);
        $this->assertEquals(
            $this->client->request['pair']['client_public_key'],
            $body['pair']['client_public_key']
        );
        $this->assertArrayHasKey('server_public_key', $body['pair']);
        // $this->assertEquals(true, $body['pair']['sign']);
        // $this->assertEquals(true, $body['pair']['encrypt']);
        $this->assertArrayHasKey('datetime', $body['pair']);
        // $this->assertArrayHasKey('filename', $body['pair']);
        // $this->assertEquals('testclient.itemsservice', $body['pair']['filename']);
        $this->assertArrayNotHasKey('shared_key', $body['pair']);

        // Send the $body as a request so that the client creates the pair file
        $client = new CreateClient($body);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}