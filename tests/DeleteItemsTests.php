<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class DeleteItemsTests extends TestCase
{
    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testDeleteItems()
    {
        // Delete the items because they are re-created in testCreateItems
        $request['action'] = 'delete items';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';

        $request['items'][] = [
            'time' => '2020-03-30 17:44:00'
        ];
        $request['items'][] = [
            'uuid' => 'UUID',
            'item' => 'item2',
            'note' => 'note2'
        ];

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        $this->assertEquals(200, $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertStringContainsString('items deleted', $body['items'][0]);
    }

    public function tearDown(): void
    {
        unset($client);
    }
}
