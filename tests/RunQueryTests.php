<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class RunQueryTests extends TestCase
{
    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testRunQuery()
    {
        $request['action'] = 'run query';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';

        $request['items'][] = [
                    'bind_sql' => 'DROP TABLE IF EXISTS `trash`;',
                    'update' => [],
                    'where' => []
                ];

        $request['items'][] = [
                    'bind_sql' =>
                        'CREATE TABLE IF NOT EXISTS `trash` (
                            `id` int(11) NOT NULL,
                            `fullname` varchar(255) NOT NULL,
                            `email` varchar(255) NOT NULL,
                            `uuid` varchar(255) NOT NULL
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;',
                    'update' => [],
                    'where' => []
                ];

        $request['items'][] = [
                    'bind_sql' =>
                        'ALTER TABLE `trash`
                         ADD PRIMARY KEY (`id`),
                         ADD UNIQUE KEY `email` (`email`),
                         ADD UNIQUE KEY `uuid` (`uuid`);',
                    'update' => [],
                    'where' => []
                ];

        $request['items'][] = [
                    'bind_sql' =>
                        'ALTER TABLE `trash`
                         MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;',
                    'update' => [],
                    'where' => []
                ];


        $request['items'][] = [
                    'bind_sql' =>
                        'INSERT IGNORE INTO `trash`
                         (`fullname`, `email`, `uuid`)
                         VALUES (:fullname, :email, :uuid);',
                    'update' => [],
                    'where' => [
                        'fullname' => 'Test Name 1',
                        'email' => 'one@test.com',
                        'uuid' => 'UUID1'
                    ]
                 ];

        $request['items'][] = [
                     'bind_sql' =>
                        'INSERT IGNORE INTO `trash` (`fullname`, `email`, `uuid`)
                         VALUES (:fullname, :email, :uuid);',
                     'update' => [],
                     'where' => [
                         'fullname' => 'Test Name 2',
                         'email' => 'two@test.com',
                         'uuid' => 'UUID2'
                     ]
                 ];

        $request['items'][] = [
                     'bind_sql' =>
                        'UPDATE `trash` SET
                         fullname = :fullname,
                         email = :email
                         WHERE uuid = :uuid;',
                     'update' => [
                         'fullname' => 'Updated Name',
                         'email' => 'updated@test.com'
                     ],
                     'where' => [
                         'uuid' => 'UUID1'
                     ]
                 ];

        $request['renew_request']['action'] = 'read items';
        $request['renew_request']['table'] = 'trash';
        $request['renew_request']['items'][] = ['uuid' => 'UUID1'];
        $request['renew_request']['limit'] = 100;
        $request['renew_request']['unset'] = ['uuid', 'id'];

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        if (isset($body['items'])
            && ! empty($body['items'])) {
            $i = 1;
            foreach ($body['items'] as $index => $item) {
                // Update the row id
                $items[$i++] = $item;
            }
        }

        $this->assertEquals(200, $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);

        // $this->assertArrayHasKey('fullname', $items[1]);
        // $this->assertArrayHasKey('email', $items[1]);
        // $this->assertEquals('Updated Name', $items[1]['fullname']);
        // $this->assertEquals('updated@test.com', $items[1]['email']);
        // $this->assertArrayNotHasKey('id', $items[1]);
        // $this->assertArrayNotHasKey('uuid', $items[1]);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
