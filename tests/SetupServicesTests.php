<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class SetupServicesTests extends TestCase
{
    private $client;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        $this->http_host='nginx1:8080';

        require_once('CreateClient.php');
    }

    public function testSetupServices()
    {
        $request['action'] = 'setup services';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';
        $request['pair']['client_url'] = "http://" . $this->http_host . "/public/index.php?=client";
        $request['pair']['server_url'] = "http://" . $this->http_host . "/public/index.php";
        
        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();
 
        $body = $response['body'];

        $this->assertEquals(201, $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);
        $this->assertEquals('ItemsService setup is complete.', $body[0]);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
