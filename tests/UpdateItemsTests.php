<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use \AllowDynamicProperties;

#[AllowDynamicProperties]
class UpdateItemsTests extends TestCase
{
    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testUpdateItems()
    {
        $request['action'] = 'update items';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'ItemsService';

        $request['items'][] = [
            'update' => ['note' => 'updated1'],
            'where' => ['uuid' => 'UUID', 'item' => 'item1']
        ];
        $request['items'][] = [
            'update' => ['time' => '2020-03-31 02:20:20'],
            'where' => ['uuid' => 'UUID', 'item' => 'item2']
        ];

        $request['renew_request']['action'] = 'read items';
        $request['renew_request']['items'][] = ['uuid' => 'UUID'];
        $request['renew_request']['limit'] = 100;
        $request['renew_request']['unset'] = ['uuid', 'id'];

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        // sayd($this->client->tag, $this->client, $response);
        $body = $response['body'];

        if (isset($body['items'])
            && ! empty($body['items'])) {
            $i = 1;
            foreach ($body['items'] as $index => $item) {
                // Update the row id
                $items[$i++] = $item;
            }
        }

        // says($response);
        $this->assertEquals(200, $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($this->client->tag, $response['tag']);

        // $this->assertArrayHasKey('item', $items[1]);
        // $this->assertArrayHasKey('note', $items[1]);
        // $this->assertEquals('item1', $items[1]['item']);
        // $this->assertEquals('updated1', $items[1]['note']);
        // $this->assertEquals('2020-03-30 17:44:00', $items[1]['time']);
        // $this->assertArrayNotHasKey('id', $items[1]);
        // $this->assertArrayNotHasKey('uuid', $items[1]);

        // $this->assertArrayHasKey('item', $items[2]);
        // $this->assertArrayHasKey('note', $items[2]);
        // $this->assertEquals('item2', $items[2]['item']);
        // $this->assertEquals('note2', $items[2]['note']);
        // $this->assertArrayNotHasKey('id', $items[2]);
        // $this->assertArrayNotHasKey('uuid', $items[2]);
        // $this->assertEquals('2020-03-31 02:20:20', $items[2]['time']);
    }

    public function tearDown(): void
    {
        unset($this->client);
    }
}
